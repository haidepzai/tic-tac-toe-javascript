const x_Class = 'x';
const circle_Class = 'circle';
const winning_combinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
];
const cellElements = document.querySelectorAll('[data-cell]');
const board = document.getElementById('board');
const endgameWindow = document.getElementById('winningMessage');
const restartButton = document.getElementById('restartButton');
const endgameText = document.querySelector('[data-winning-message-text]');
let playerTurnMessage = document.getElementById('playerTurn');
let circleTurn;

startGame();

restartButton.addEventListener('click', startGame);

function startGame() {
    circleTurn = false; //Player X Start
    cellElements.forEach(cell => {  //alles resetten                
        cell.classList.remove(x_Class);
        cell.classList.remove(circle_Class);
        cell.removeEventListener('click', handleClick);     //EventListener nur einmal ausführbar
        cell.addEventListener('click', handleClick, {once: true}); //Nur einmal klickbar
    }); //Jedes Feld ein EventListener hinzufügen
    setBoardHoverClass();
    endgameWindow.classList.remove('show');
    playerTurnMessage.innerHTML = "Player X Turn";
}

function handleClick(e) {
    //TODO: placeMark
    //Check for Win
    //Check for Draw
    //Switch Turns
    
    const cell = e.target; //Target = Referenz auf das Object, welches das Event auslöst
    const currentClass = circleTurn ? circle_Class : x_Class;
    placeMark(cell, currentClass);
    if (checkWin(currentClass)){
        hasWinner(true);
    } else if (isDraw()) {
        hasWinner(false);
    } else {
        swapTurns();
        setBoardHoverClass();
        setPlayersTurnMessage(currentClass);
    }
}

function hasWinner(winner){
    if (winner){
        endgameText.innerText = `${circleTurn ? "O has" : "X has"} Won!`
    } else {
        endgameText.innerText = 'Draw!';        
    }
    endgameWindow.classList.add('show');
}
//Falls Alle Felder besetzt sind
function isDraw(){ //Destructor
    return [...cellElements].every(cell => {
        return cell.classList.contains(x_Class) ||
            cell.classList.contains(circle_Class)
    });
}

//Mittels DOM Manipulation Klassen hinzufügen ins HTML, je nachdem welcher Spieler am Zug ist
function placeMark(cell, currentClass){
    cell.classList.add(currentClass);
    
    /*if(currentClass === x_Class){
        cell.innerHTML = "X";
    } else if(currentClass === circle_Class){
        cell.innerHTML = "O";
    }*/
}

function swapTurns() {
    circleTurn = !circleTurn;
}

function setPlayersTurnMessage(currentClass){
    if (!circleTurn){
        playerTurnMessage.innerHTML = "Player X Turn";
    } else {
        playerTurnMessage.innerHTML = "Player O Turn";
    }
}
//Fügt eine Klasse ins board-div hinzu (Löscht zunächst bereits vorhandene Klassen)
function setBoardHoverClass() {
    board.classList.remove(x_Class);
    board.classList.remove(circle_Class);
    
    if(circleTurn) {
        board.classList.add(circle_Class);
    } else {
        board.classList.add(x_Class);
    }
    
}

function checkWin(currentClass){
    //Loop jede Kombination
    return winning_combinations.some(combination => { //some = überprüft, ob mindestens ein Element des Arrays true ist
        return combination.every(index => { //every = ob alle Elemente in einem Array true sind (alle Elemente gleiche Klasse)
            return cellElements[index].classList.contains(currentClass); 
        });
    });
}